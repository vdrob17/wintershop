<?php


namespace Elogic\Device\Api\Data;


interface DeviceInterface
{
    const DEVICE_ID = 'device_id';
    const DEVICE_NAME = 'device_name';
    const DEVICE_OC = 'device_oc';
    const DEVICE_PRICE = 'device_price';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME = 'update_time';
    const IS_USED = 'is_used';

    /**
     * get ID
     * @return int
     */
    public function getId();

    /**
     * get Device name
     * @return string
     */
    public function getDeviceName();

    /**
     * get Device OC
     * @return string
     */
    public function getDeviceOc();

    /**
     * get Device Price
     * @return int
     */
    public function getDevicePrice();

    /**
     * get Creation Time
     * @return string
     */
    public function getCreationTime();
    /**
     * get Update Time
     * @return string
     */
    public function getUpdateTime();

    /**
     * Get is used
     * @return bool true or false
     */
    public function getIsUsed();

    /**
     * @param $id
     * @return DeviceInterface
     */
    public function setId($id);
    /**
     * Set ID
     * @param $id
     * @return DeviceInterface
     */
    public function setDeviceName($deviceName);
    /**
     * Set Name
     * @param $deviceName
     * @return DeviceInterface
     */
    public function setDeviceOc($deviceOc);
    /**
     * Set OC
     * @param $deviceOc
     * @return DeviceInterface
     */
    public function setDevicePrice($devicePrice);
    /**
     * Set Price
     * @param $devicePrice
     * @return DeviceInterface
     */
    public function setCreationTime($creationTime);
    /**
     * Set Creation Time
     * @param $creationTime
     * @return DeviceInterface
     */
    public function setUpdateTime($updateTime);
    /**
     * Set Update Time
     * @param $updateTime
     * @return DeviceInterface
     */
    public function setIsUsed($isUsed);
    /**
     * Set is used
     * @param $isUsed
     * @return DeviceInterface
     */
}