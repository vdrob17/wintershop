<?php


namespace Elogic\Device\Api;


interface DeviceRepositoryInterface
{
    /**
     * Save block.
     *
     * @param \Elogic\Device\Api\Data\DeviceInterface $device
     * @return \Elogic\Device\Api\Data\DeviceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\DeviceInterface $device);

    /**
     * Retrieve block.
     *
     * @param int $device
     * @return \Elogic\Device\Api\Data\DeviceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($device);

    /**
     * Retrieve blocks matching the specified criteria.
     *
     * @param \Elogic\Review\Api\Data\DeviceInterface $device
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function delete(Data\DeviceInterface $device);

    /**
     * Delete block by ID.
     *
     * @param int $device
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($device);
}