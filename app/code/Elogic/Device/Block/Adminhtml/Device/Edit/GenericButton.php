<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Elogic\Device\Block\Adminhtml\Device\Edit;
use Magento\Backend\Block\Widget\Context;
use Elogic\Device\Api\DeviceRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;

    /**
     * @var BlockRepositoryInterface
     */
    protected $deviceReviewRepository;

    /**
     * @param Context $context
     * @param BlockRepositoryInterface $deviceRepository
     */
    public function __construct(
        Context $context,
        DeviceRepositoryInterface $deviceReviewRepository
    ) {
        $this->context = $context;
        $this->deviceReviewRepository = $deviceReviewRepository;
    }

    /**
     * Return Review ID
     *
     * @return int|null
     */
    public function getReviewId()
    {
        if ($this->context->getRequest()->getParam('device_id')) {
            try {
                return $this->deviceReviewRepository->getById(
                    $this->context->getRequest()->getParam('device_id')
                )->getId();
            } catch (NoSuchEntityException $e) {
            }
            return null;
        }
    }
    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
