<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Elogic\Device\Controller\adminhtml\Device;

use Elogic\Device\Api\DeviceRepositoryInterface;
use Elogic\Device\Model\Device;
use Elogic\Device\Model\DeviceRepository;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Elogic_Device::view';

    private $deviceRepository;
    public function __construct(Action\Context $context,
                                DeviceRepositoryInterface $deviceRepository)
    {
        $this->deviceRepository = $deviceRepository;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('device_id');
        if ($id) {
            try {
                $this->deviceRepository->deleteById($id);

                $this->messageManager->addSuccessMessage(__('You deleted the device.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/');
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a device to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
