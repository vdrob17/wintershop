<?php


namespace Elogic\Device\Controller\adminhtml\Device;



use Elogic\Device\Api\DeviceRepositoryInterface;
use Elogic\Device\Model\Device;
use Elogic\Device\Model\DeviceFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;

/**
 * Edit CMS block action.
 */
class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $deviceRepository;
    protected $deviceFactory;
    protected $coreRegistry;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        DeviceRepositoryInterface $deviceRepository,
        DeviceFactory $deviceFactory
    ) {
        $this->deviceFactory = $deviceFactory;
        $this->deviceRepository = $deviceRepository;
        $this->resultPageFactory = $resultPageFactory;
        $this->coreRegistry = $coreRegistry;
        parent::__construct($context);
    }

    /**
     * Edit CMS block
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('device_id');

        // 2. Initial checking
        if ($id) {
            $model= $this->deviceRepository->getById($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This block no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }

        }
        else{
            $model = $this->deviceFactory->create();
        }
        $this->coreRegistry->register('store_review', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Elogic_Device::device')
            ->addBreadcrumb(__('Device'), __('Device'));
        $resultPage->getConfig()->getTitle()->prepend(__('Devices'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getId() : __('New Device'));
        return $resultPage;
    }
}
