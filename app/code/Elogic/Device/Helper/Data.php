<?php


namespace Elogic\Device\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;

class Data
{
    const ELOGIC_DEVICE_SET_DEVICE_USED_DEVICE_USED = "set_device/used_device/used";
    private $scopeConfig;

    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    public function getUsedDevice()
    {
        return $this->scopeConfig->getValue(self::ELOGIC_DEVICE_SET_DEVICE_USED_DEVICE_USED);
    }
}