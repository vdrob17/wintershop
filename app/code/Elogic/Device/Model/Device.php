<?php


namespace Elogic\Device\Model;


use Elogic\Device\Api\Data\DeviceInterface;
use Magento\Framework\Model\AbstractModel;

class Device extends AbstractModel implements DeviceInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Elogic\Device\Model\ResourceModel\Device::class);
    }
    public function getId()
    {
        return $this->getData(self::DEVICE_ID);
    }

    /**
     * get Device name
     * @return string
     */
    public function getDeviceName()
    {
        return $this->getData(self::DEVICE_NAME);
    }

    /**
     * get Device OC
     * @return string
     */
    public function getDeviceOc()
    {
        return $this->getData(self::DEVICE_OC);
    }
    /**
     * get Device Price
     * @return int
     */
    public function getDevicePrice()
    {
        return $this->getData(self::DEVICE_PRICE);
    }

    /**
     * get Creation Time
     * @return string
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }
    /**
     * get Update Time
     * @return string
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Get is used
     * @return bool true or false
     */
    public function getIsUsed()
    {
        return $this->getData(self::IS_USED);
    }

    /**
     * @param $id
     * @return DeviceInterface
     */
    public function setId($id)
    {
        return $this->setData(self::DEVICE_ID,$id);
    }
    /**
     * Set ID
     * @param $id
     * @return DeviceInterface
     */
    public function setDeviceName($deviceName)
    {
        return $this->setData(self::DEVICE_NAME,$deviceName);
    }
    /**
     * Set Name
     * @param $deviceName
     * @return DeviceInterface
     */
    public function setDeviceOc($deviceOc)
    {
        return $this->setData(self::DEVICE_OC,$deviceOc);
    }
    /**
     * Set OC
     * @param $deviceOc
     * @return DeviceInterface
     */
    public function setDevicePrice($devicePrice)
    {
        return $this->setData(self::DEVICE_PRICE,$devicePrice);
    }
    /**
     * Set Price
     * @param $devicePrice
     * @return DeviceInterface
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(self::CREATION_TIME,$creationTime);
    }
    /**
     * Set Creation Time
     * @param $creationTime
     * @return DeviceInterface
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(self::UPDATE_TIME,$updateTime);
    }
    /**
     * Set Update Time
     * @param $updateTime
     * @return DeviceInterface
     */
    public function setIsUsed($isUsed)
    {
        return $this->setData(self::IS_USED,$isUsed);
    }
    /**
     * Set is used
     * @param $isUsed
     * @return DeviceInterface
     */
}