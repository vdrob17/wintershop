<?php


namespace Elogic\Device\Model;


use Elogic\Device\Api\DeviceRepositoryInterface;
use Elogic\Device\Model\ResourceModel\Device as ResourceDevice;
use Elogic\Device\Api\Data;
use Magento\Framework\Exception\CouldNotSaveException;

class DeviceRepository implements DeviceRepositoryInterface
{

    /**
     * @var ResourceDevice
     */
    protected $resource;

    /**
     * @var DeviceFactory
     */
    protected $deviceFactory;

    /**
     * StoreReviewRepository constructor.
     * @param ResourceDevice $resourceDevice
     * @param DeviceFactory $deviceFactory
     */
    public function __construct(
        ResourceDevice $resourceDevice,
        DeviceFactory $deviceFactory
    )
    {
        $this->resource = $resourceDevice;
        $this->deviceFactory = $deviceFactory;
    }

    /**
     * Save store review.
     *
     * @param \Elogic\Device\Api\Data\DeviceInterface $device
     * @return \Elogic\Device\Api\Data\DeviceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\DeviceInterface $device)
    {
        try {
            /** @var $device \Elogic\Device\ModelDevice\ */
            $this->resource->save($device);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $device;
    }

    /**
     * Retrieve store review.
     *
     * @param int $deviceId
     * @return \Elogic\Device\Api\Data\DeviceInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($deviceId)
    {
        $device = $this->deviceFactory->create();
        $this->resource->load($device, $deviceId);
        if (!$device->getId()) {
            throw new NoSuchEntityException(__('The CMS block with the "%1" ID doesn\'t exist.', $deviceId));
        }
        return $device;
    }

    /**
     * Delete store review.
     *
     * @param \Elogic\Device\Api\Data\DeviceInterface $device
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\DeviceInterface $device)
    {
        try {
            /** @var $device \Elogic\Device\Model\Device */
            $this->resource->delete($device);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete store review by ID.
     *
     * @param int $device
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($device)
    {
        return $this->delete($this->getById($device));
    }
}