<?php


namespace Elogic\Device\Model\ResourceModel;
use Magento\Framework\Model\ResourceModel\Db\AbstractDb;


class Device extends AbstractDb
{
    /**
     * Initialize resource model
     * @return void
     */
    protected function _construct()
    {
        $this->_init('device', 'device_id');
    }
}