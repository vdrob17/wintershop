<?php


namespace Elogic\Device\Model\ResourceModel\Device;


use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    protected $_idFieldName = 'device_id';
    protected function _construct()
    {
        $this->_init(\Elogic\Device\Model\Device::class, \Elogic\Device\Model\ResourceModel\Device::class);

    }

}