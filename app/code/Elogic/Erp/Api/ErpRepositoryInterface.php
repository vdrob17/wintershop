<?php

namespace Elogic\Erp\Api;

use Elogic\Erp\Model\ErpRepository;

interface ErpRepositoryInterface
{

    /**
     * Save Erp.
     *
     * @param \Elogic\Erp\Api\Data\ErpInterface $erp
     * @return \Elogic\Erp\Api\Data\ErpInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\ErpInterface $erp);

    /**
     * Retrieve Erp.
     *
     * @param int $id
     * @return \Elogic\Erp\Api\Data\ErpInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve blocks matching the specified criteria.
     *
     * @param \Elogic\Erp\Api\Data\ErpInterface $erp
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function delete(Data\ErpInterface $erp);

    /**
     * Delete block by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);

    /**
     * @param int $orderId
     * @return ErpRepositoryInterface
     */
    public function getErpByOrderId($orderId);

}
