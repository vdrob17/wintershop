<?php

namespace Elogic\Erp\Model;

use Elogic\Erp\Api\Data;
use Elogic\Erp\Api\ErpRepositoryInterface;
use Elogic\Erp\Model\ResourceModel\Erp as ResourceErp;
use Magento\Framework\Exception\CouldNotSaveException;

class ErpRepository implements ErpRepositoryInterface
{
    protected $resource;

    /**
     * @var ErpFactory\
     */
    protected $erpFactory;

    /**
     * StoreReviewRepository constructor.
     * @param ResourceErp $resourceErp
     * @param ErpFactory $erpFactory
     */
    public function __construct(
        ResourceErp $resourceErp,
        ErpFactory $erpFactory
    ) {
        $this->resource = $resourceErp;
        $this->erpFactory = $erpFactory;
    }

    /**
     * Save store review.
     *
     * @param \Elogic\Erp\Api\Data\ErpInterface $erp
     * @return \Elogic\Erp\Api\Data\ErpInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\ErpInterface $erp)
    {
        try {
            /** @var $erp \Elogic\Erp\Model\Erp */
            $this->resource->save($erp);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__($exception->getMessage()));
        }
        return $erp;
    }

    /**
     * Retrieve store review.
     *
     * @param int $id
     * @return \Elogic\Review\Api\Data\StoreReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id)
    {
        $erp = $this->erpFactory->create();
        $this->resource->load($erp, $id);
        if (!$erp->getId()) {
            throw new NoSuchEntityException(__('The erp entity with the "%1" ID doesn\'t exist.', $id));
        }
        return $erp;
    }

    /**
     * Delete store review.
     *
     * @param \Elogic\Review\Api\Data\StoreReviewInterface $storeReview
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(Data\ErpInterface $erp)
    {
        try {
            /** @var $erp \Elogic\Erp\Model\Erp */
            $this->resource->delete($erp);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * Delete store review by ID.
     *
     * @param int $id
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * @param $orderId
     * @return Data\ErpInterface
     */
    public function getErpByOrderId($orderId)
    {
        $erp = $this->erpFactory->create();
        $this->resource->load($erp, $orderId, Data\ErpInterface::ORDER_ID);
        return $erp;
    }
}
