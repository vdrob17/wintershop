<?php

namespace Elogic\Erp\Plugin;

use Elogic\Erp\Api\ErpRepositoryInterface;
use Magento\Framework\Exception\CouldNotSaveException;

class OrderSave
{
    /**
     * @var ErpRepositoryInterface
     */
    private $erpRepository;

    /**
     * OrderSave constructor.
     * @param ErpRepositoryInterface $erpRepository
     */
    public function __construct(
        ErpRepositoryInterface $erpRepository
    ) {
        $this->erpRepository = $erpRepository;
    }

    /**
     * @param \Magento\Sales\Api\OrderRepositoryInterface $subject
     * @param \Magento\Sales\Api\Data\OrderInterface $resultOrder
     * @return \Magento\Sales\Api\Data\OrderInterface
     * @throws CouldNotSaveException
     */
    public function afterSave(
        \Magento\Sales\Api\OrderRepositoryInterface $subject,
        \Magento\Sales\Api\Data\OrderInterface $resultOrder
    ) {
        $resultOrder = $this->saveErpAttribute($resultOrder);

        return $resultOrder;
    }

    private function saveErpAttribute(\Magento\Sales\Api\Data\OrderInterface $order)
    {
        $extensionAttributes = $order->getExtensionAttributes();
        if (
            null !== $extensionAttributes &&
            null !== $extensionAttributes->getErpAttribute()
        ) {
            $erpAttribute = $extensionAttributes->getErpAttribute();
            try {
                $erp = $this->erpRepository->getErpByOrderId($order->getEntityId());
                $erp->setOrderId($order->getEntityId())->setErpId($erpAttribute->getErpId());
                $this->erpRepository->save($erp);
            } catch (\Exception $e) {
                throw new CouldNotSaveException(
                    __('Could not add attribute to order: "%1"', $e->getMessage()),
                    $e
                );
            }
        }
        return $order;
    }
}
