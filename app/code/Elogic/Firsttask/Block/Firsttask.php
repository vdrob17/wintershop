<?php

namespace Elogic\Firsttask\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\View\Element\Template;

class Firsttask extends Template
{
    private $productRepository;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        Template\Context $context,
        array $data = [])
    {
        parent::__construct($context, $data);
        $this->productRepository = $productRepository;
    }

    public function getFirstProductName()
    {
        $product = $this->productRepository->getById(1);
        if ($product->getId())
        {
            return $product->getName();
        }
        return '';
    }


}