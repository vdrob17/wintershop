<?php
namespace Elogic\Firsttask\Controller\Index;

use Elogic\Erp\Model\ErpFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Sales\Api\OrderRepositoryInterface;

class Index extends Action implements HttpGetActionInterface, HttpPostActionInterface
{
    /**
    * @var ErpFactory
    */
    private $erpFactory;
    /**
    * @var OrderRepositoryInterface
    */
    private $orderRepository;

    /**
    * Myaction constructor.
    * @param ErpFactory $erpFactory
    * @param OrderRepositoryInterface $orderRepository
    * @param Context $context
    */
    public function __construct(
        ErpFactory $erpFactory,
        OrderRepositoryInterface $orderRepository,
        Context $context
    ) {
        parent::__construct($context);
        $this->erpFactory = $erpFactory;
        $this->orderRepository = $orderRepository;
    }

    public function execute()
    {
//        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
//
//        $order = $objectManager->create('Magento\Sales\Model\Order')->load(1);

        $order = $this->orderRepository->get(1);

        $erp = $this->erpFactory->create();
        $erp->setErpId("sfdsfsfdsfdsfdsf");

        $order->getExtensionAttributes()->setErpAttribute($erp);
//        $erp = $order->getExtensionAttributes()->getErpAttribute();
//
//        var_dump($erp->getData());
        $order->save();
        $this->orderRepository->save($order);
        echo "Mycontroller myaction";
    }
}
