<?php

namespace Elogic\Review\Api\Data;


interface StoreReviewInterface
{
    const REVIEW_ID = 'review_id';
    const CUSTOMER_NAME = 'customer_name';
    const CONTENT = 'content';
    const CREATION_TIME = 'creation_time';
    const UPDATE_TIME = 'update_time';
    const IS_ACTIVE = 'is_active';

    /**
     * Get ID
     *
     * @return int|null;
     */
    public function getId();

    /**
     * GetCustomerName
     * @return string;
     */
    public function getCustomerName();


    /**
     *  Get Content
     * @return string|null;
     */
    public function getContent();

    /**
     * Get Creation time
     * @return string|null;
     */
    public function getCreationTime();

    /**
     * Get update time
     * @return string|null
     */
    public function getUpdateTime();

    /**
     * Get isActive
     * @return string|null
     */
    public function getIsActive();

    /**
     * @param $id
     * @return int
     * Set ID
     */
    public function setId($id);

    /**
     * @param $customerName
     * @return StoreReviewInterface
     * Set customer name
     */
    public function setCustomerName($customerName);

    /**
     * @param $content
     *@return StoreReviewInterface
     * Set Content
     */
    public function setContent($content);

    /**
     * @param $creationTime
     * @return StoreReviewInterface
     * Set creation time
     */
    public function setCreationTime($creationTime);

    /**
     * @param $updateTime
     * @return StoreReviewInterface
     * Set Update Time
     */
    public function setUpdateTime($updateTime);

    /**
     * @return StoreReviewInterface
     * @param $isActive
     * Set isActive
     */
    public function setIsActive($isActive);
}
