<?php

namespace Elogic\Review\Api;


interface StoreRepositoryInterface
{
    /**
     * Save block.
     *
     * @param \Elogic\Review\Api\Data\StoreReviewInterface $storeReview
     * @return \Elogic\Review\Api\Data\StoreReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(Data\StoreReviewInterface $storeReview);

    /**
     * Retrieve block.
     *
     * @param int $storeReviewId
     * @return \Elogic\Review\Api\Data\StoreReviewInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($storeReviewId);

    /**
     * Retrieve blocks matching the specified criteria.
     *
     * @param \Elogic\Review\Api\Data\StoreReviewInterface $StoreReview
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */

    public function delete(Data\StoreReviewInterface $StoreReview);

    /**
     * Delete block by ID.
     *
     * @param int $storeReviewId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($storeReviewId);
}
