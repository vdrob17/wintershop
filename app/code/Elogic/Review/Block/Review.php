<?php

namespace Elogic\Review\Block;

use Elogic\Review\Api\Data\StoreReviewInterface;
use Elogic\Review\Api\StoreRepositoryInterface;
use Elogic\Review\Model\StoreReview;
use Elogic\Review\Model\StoreReviewFactory;
use Magento\Framework\View\Element\Template;

class Review extends Template
{
    /**
     * @var StoreRepositoryInterface
     */
    private $reviewRepository;
    /**
     * @var StoreReviewFactory
     */
    private $storeReviewFactory;
    /**
     * @var StoreReviewInterface
     */
    private $storeReview;
    /**
     * @var \Elogic\Review\Model\Url
     */
    private $reviewUrl;

    /**
     * Review constructor.
     * @param StoreRepositoryInterface $reviewRepository
     * @param Template\Context $context
     * @param StoreReviewFactory $storeReviewFactory
     * @param StoreReviewInterface $storeReview
     * @param \Elogic\Review\Model\Url $reviewUrl
     * @param array $data
     */
    public function __construct(
        StoreRepositoryInterface $reviewRepository,
        Template\Context $context,
        StoreReviewFactory $storeReviewFactory,
        StoreReviewInterface $storeReview,
        \Elogic\Review\Model\Url $reviewUrl,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->reviewRepository = $reviewRepository;
        $this->storeReview = $storeReview;
        $this->storeReviewFactory = $storeReviewFactory;
        $this->reviewUrl = $reviewUrl;
    }

    /** @var StoreReview $storeReview */
    /**
     *
     * @return int
     */
    public function getNumPage()
    {
        return $this
            ->getRequest()
            ->getParam('page');
    }

    /**
     * @return array
     */
    public function getReviews()
    {
        $numPage = $this->getNumPage();
        $collection = $this
            ->getActiveReviews();
        $customer = $this
            ->getCustomer();
        if ($customer != null) {
            $collection
                ->addFieldToFilter('customer_name', [
                    ['like' => '% ' . $customer . ' %'], //spaces on each side
                    ['like' => '% ' . $customer], //space before and ends with $needle
                    ['like' => $customer . ' %'],
                    ['like' => $customer]// starts with needle and space after
                ]);
        }
        $collection
            ->getSelect()
            ->reset(\Magento\Framework\DB\Select::LIMIT_COUNT)
            ->limitPage($numPage, 5);
        return $collection->getData();
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        if ($this->getRequest()->getParam('customer_name')) {
            return $this->getRequest()->getParam('customer_name');
        } else {
            return $this->getRequest()->getParam('customer');
        }
    }
    /**
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     */
    public function getActiveReviews()
    {
        return $collection = $this->storeReviewFactory
            ->create()
            ->getCollection()
            ->setOrder('creation_time', \Zend_Db_Select::SQL_DESC)
            ->addFieldToFilter('is_active', ['eq' => 1]);
    }

    /**
     * @return string
     */
    public function getFormUrl()
    {
        return $this->reviewUrl->getFormUrl();
    }

    /**
     * @return int
     */
    public function getCount()
    {
        $customer = $this->getCustomer();
        $collection = $this->storeReviewFactory
            ->create()
            ->getCollection()
            ->addFieldToFilter('is_active', ['eq' => 1]);
        if ($customer != null) {
            $collection
                ->addFieldToFilter('customer_name', [
                    ['like' => '% ' . $customer . ' %'], //spaces on each side
                    ['like' => '% ' . $customer], //space before and ends with $needle
                    ['like' => $customer . ' %'],
                    ['like' => $customer]// starts with needle and space after
                ]);
        }
        return $collection->count();
    }

    /**
     * @param $i
     * @return string
     */
    public function getPageUrl($i)
    {
        $customer = $this->getCustomer();
        if ($i >= 0 || $i<= $this->getCountPages()) {
            return $this->reviewUrl->getPageUrl($i, $customer);
        }
        return $this->reviewUrl->getPageUrl(1, $customer);
    }
    public function getFirstPageUrl()
    {
        return $this->reviewUrl->getPageUrl(1);
    }
    /**
     * @return false|float
     */
    public function getCountPages()
    {
        return ceil($this->getCount() / 5);
    }
}
