<?php

namespace Elogic\Review\Block;

use Magento\Framework\View\Element\Template;

class Save extends Template
{
    /**
     * @var \Elogic\Review\Model\Url
     */
    private $reviewUrl;

    /**
     * Save constructor.
     * @param Template\Context $context
     * @param \Elogic\Review\Model\Url $reviewUrl
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        \Elogic\Review\Model\Url $reviewUrl,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->reviewUrl = $reviewUrl;
    }

    /**
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->reviewUrl->getSaveUrl();
    }
}
