<?php

namespace Elogic\Review\Controller\Form;

use Elogic\Review\Api\Data\StoreReviewInterface;
use Elogic\Review\Api\StoreRepositoryInterface;
use Elogic\Review\Model\StoreReviewFactory;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Save extends Action implements HttpGetActionInterface, HttpPostActionInterface
{
    protected $storeReviewFactory;
    protected $storeReviewRepository;
    protected $storeReview;

    public function __construct(
        StoreReviewFactory $storeReviewFactory,
        StoreRepositoryInterface $storeReviewRepository,
        StoreReviewInterface $storeReview,
        Context $context
    ) {
        $this->storeReviewFactory = $storeReviewFactory;
        $this->storeReviewRepository = $storeReviewRepository;
        $this->storeReview = $storeReview;

        parent::__construct($context);
    }

    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $post = $this->getRequest()->getPostValue();
        if ($post['customer_name']!= '' && $post['content']!='') {
            $review = $this->storeReviewFactory->create();
            $review->setData($post);
            $this->storeReviewRepository->save($review);
            $this->messageManager->addSuccessMessage(__('You add the review.'));
            return $resultRedirect->setPath('*/');
        } else {
            $this->messageManager->addErrorMessage(__('Sorry something went wrong'));
            return $resultRedirect->setPath('*/');
        }
    }
}
