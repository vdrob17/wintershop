<?php
/**
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Elogic\Review\Controller\Adminhtml\Review;

use Elogic\Review\Api\StoreRepositoryInterface;
use Elogic\Review\Model\StoreReview;
use Elogic\Review\Model\StoreReviewRepository;
use Magento\Backend\App\Action;
use Magento\Framework\App\Action\HttpPostActionInterface;

class Delete extends \Magento\Backend\App\Action
{
    /**
     * Authorization level of a basic admin session
     *
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Elogic_Review::view';

    private $storeRepository;
    public function __construct(Action\Context $context,
                                StoreRepositoryInterface $storeRepository)
    {

        $this->storeRepository = $storeRepository;
        parent::__construct($context);
    }

    /**
     * Delete action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        // check if we know what should be deleted
        $id = $this->getRequest()->getParam('review_id');
        if ($id) {
            try {
                $this->storeRepository->deleteById($id);

                $this->messageManager->addSuccessMessage(__('You deleted the review.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addErrorMessage($e->getMessage());
                // go back to edit form
                return $resultRedirect->setPath('*/*/');
            }
        }
        // display error message
        $this->messageManager->addErrorMessage(__('We can\'t find a review to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
