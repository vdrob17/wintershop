<?php


namespace Elogic\Review\Controller\adminhtml\Review;



use Elogic\Review\Api\StoreRepositoryInterface;
use Elogic\Review\Model\StoreReview;
use Elogic\Review\Model\StoreReviewFactory;
use Magento\Framework\App\Action\HttpGetActionInterface;

/**
 * Edit CMS block action.
 */
class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $storeReveiwRepository;
    protected $storeReviewFactory;
    protected $coreRegistry;
    /**
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        StoreRepositoryInterface $storeReveiwRepository,
        StoreReviewFactory $storeReviewFactory
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->storeReviewFactory = $storeReviewFactory;
        $this->storeReveiwRepository = $storeReveiwRepository;
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    /**
     * Edit CMS block
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('review_id');

        // 2. Initial checking
        if ($id) {
            $model= $this->storeReveiwRepository->getById($id);
            if (!$model->getId()) {
                $this->messageManager->addErrorMessage(__('This review no longer exists.'));
                /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }

        }
        else{
            $model = $this->storeReviewFactory->create();
        }
        $this->coreRegistry->register('store_review', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->setActiveMenu('Elogic_Review::review')
            ->addBreadcrumb(__('StoreReview'), __('StoreReview'));
        $resultPage->getConfig()->getTitle()->prepend(__('Reviews'));
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? $model->getId() : __('New Review'));
        return $resultPage;
    }
}
