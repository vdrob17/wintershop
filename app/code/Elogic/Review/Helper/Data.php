<?php

namespace Elogic\Review\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Data
{
    const ELOGIC_REVIEW_SETTINGS_GENERAL_ENABLED = "settings/general/enabled";
    const ELOGIC_REVIEW_SETTINGS_GENERAL_ALLOW_POST = "settings/general/allow_post";
    const ELOGIC_REVIEW_SETTINGS_GENERAL_PREMODERATE = "settings/general/premoderate";
    private $scopeConfig;
    public function __construct(ScopeConfigInterface $scopeConfig)
    {
        $this->scopeConfig = $scopeConfig;
    }

    /**
     * @return ScopeConfigInterface
     */
    public function getStoreReviewEnabled()
    {
        return $this->scopeConfig->getValue(self::ELOGIC_REVIEW_SETTINGS_GENERAL_ENABLED);
    }
    public function getStoreReviewAllowPost()
    {
        return $this->scopeConfig->getValue(self::ELOGIC_REVIEW_SETTINGS_GENERAL_ALLOW_POST);
    }
    public function getStoreReviewPremoderate()
    {
        return $this->scopeConfig->getValue(self::ELOGIC_REVIEW_SETTINGS_GENERAL_PREMODERATE);
    }
}
