<?php


namespace Elogic\Review\Model;


use Elogic\Review\Api\Data\StoreReviewInterface;
use Magento\Cms\Api\Data\BlockInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class StoreReview extends AbstractModel implements StoreReviewInterface
{
    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;
    protected $_eventPrefix = "store_review";
    /**
     * Construct
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Elogic\Review\Model\ResourceModel\StoreReview::class);
    }
    /**
     * Retrieve Store Review id
     *
     * @return int
     */
    public function getId()
    {
        return $this->getData(self::REVIEW_ID);
    }

    /**
     * Retrieve Customer Name
     * @return string
     */
    public function getCustomerName()
    {
        return $this->getData(self::CUSTOMER_NAME);
    }

    /**
     * Get Content
     * @return mixed|string|null
     */
    public function getContent()
    {
        return $this->getData(self::CONTENT);
    }

    /**
     * Get Creation Time
     * @return mixed|string|null
     */
    public function getCreationTime()
    {
        return $this->getData(self::CREATION_TIME);
    }

    /**
     * Get Update Time
     * @return mixed|string|null
     */
    public function getUpdateTime()
    {
        return $this->getData(self::UPDATE_TIME);
    }

    /**
     * Get is Active
     * @return bool
     */
    public function getIsActive()
    {

        return (bool)$this->getData(self::IS_ACTIVE);
    }

    /**
     * Set ID
     * @param mixed $id
     * @return StoreReviewInterface
     */
    public function setId($id)
    {
        return $this->setData(self::REVIEW_ID, $id);
    }

    /**
     * Set customer name
     * @param $customerName
     * @return StoreReviewInterface
     */
    public function setCustomerName($customerName)
    {
        return $this->setData(self::CUSTOMER_NAME, $customerName);
    }

    /**
     * Set Content
     * @param $content
     * @return StoreReviewInterface|mixed
     */
    public function setContent($content)
    {
        return $this->setData(self::CONTENT, $content);
    }

    /**
     * Set Creation Time
     * @param $creationTime
     * @return StoreReviewInterface|mixed
     */
    public function setCreationTime($creationTime)
    {
        return $this->setData(self::CREATION_TIME, $creationTime);
    }

    /**
     * Set Update Time
     * @param $updateTime
     * @return StoreReviewInterface
     */
    public function setUpdateTime($updateTime)
    {
        return $this->setData(self::UPDATE_TIME, $updateTime);
    }

    /**
     * Set isActive
     * @param $isActive
     * @return StoreReviewInterface
     */
    public function setIsActive($isActive)
    {

        return $this->setData(self::IS_ACTIVE, $isActive);
    }
}