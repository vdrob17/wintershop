<?php

namespace Elogic\Review\Model;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Url\EncoderInterface;
use Magento\Framework\UrlInterface;

class Url
{

    /**
     * @var UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var RequestInterface
     */
    protected $request;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var Session
     */
    protected $customerSession;

    /**
     * @var EncoderInterface
     */
    protected $urlEncoder;

    /**
     * @var \Magento\Framework\Url\DecoderInterface
     */
    private $urlDecoder;

    /**
     * @var \Magento\Framework\Url\HostChecker
     */
    private $hostChecker;

    /**
     * @param Session $customerSession
     * @param ScopeConfigInterface $scopeConfig
     * @param RequestInterface $request
     * @param UrlInterface $urlBuilder
     * @param EncoderInterface $urlEncoder
     * @param \Magento\Framework\Url\DecoderInterface|null $urlDecoder
     * @param \Magento\Framework\Url\HostChecker|null $hostChecker
     */
    public function __construct(
        Session $customerSession,
        ScopeConfigInterface $scopeConfig,
        RequestInterface $request,
        UrlInterface $urlBuilder,
        EncoderInterface $urlEncoder,
        \Magento\Framework\Url\DecoderInterface $urlDecoder = null,
        \Magento\Framework\Url\HostChecker $hostChecker = null
    )
    {
        $this->request = $request;
        $this->urlBuilder = $urlBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->customerSession = $customerSession;
        $this->urlEncoder = $urlEncoder;
        $this->urlDecoder = $urlDecoder ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Url\DecoderInterface::class);
        $this->hostChecker = $hostChecker ?: \Magento\Framework\App\ObjectManager::getInstance()
            ->get(\Magento\Framework\Url\HostChecker::class);
    }

    /**
     * @return string
     */
    public function getFormUrl()
    {
        return $this->urlBuilder->getUrl('reviews/form');
    }

    /**
     * Retrieve customer register form url
     *
     * @return string
     */
    public function getSaveUrl()
    {
        return $this->urlBuilder->getUrl('reviews/form/save');
    }

    /**
     * @param $param
     * @return string
     */
    public function getPageUrl($page,$customer=null)
    {
        if ($customer != null) {
            return $this->urlBuilder->getUrl('reviews/view/', ['page' => $page, 'customer' => $customer]);
        }
        else {
            return $this->urlBuilder->getUrl('reviews/view/', ['page' => $page]);
        }
    }
}
